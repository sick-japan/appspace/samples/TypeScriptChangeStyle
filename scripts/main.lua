--luacheck: ignore Script

Script.serveEvent("TypeScriptChangeStyle.displayText", "displayText")

local function main()
  Script.notifyEvent("displayText", "blue")
end
Script.register("Engine.OnStarted", main)

local function onChangedColorButton(change)
  Script.notifyEvent("displayText", change)
end
Script.serveFunction("TypeScriptChangeStyle.onChangedColorButton", onChangedColorButton)

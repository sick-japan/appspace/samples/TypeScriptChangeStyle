export function colorChange(value) {
  if (value == "blue") {
    var elem = document.getElementById('ValueDisplay');
    elem.style.color = 'blue';
  } else if (value == "red") {
    var elem = document.getElementById('ValueDisplay');
    elem.style.color = 'red';
  }
  return value;
}